import os
import json
from datetime import datetime
import unittest

from bs4 import BeautifulSoup

try:
    from splinter import Browser, Config
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.common.action_chains import ActionChains
except ImportError:
    # on uberspace we do not want to install a (headless) browser
    execute_browser_tests = False
else:
    execute_browser_tests = True

from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from django.conf import settings
from base import models

from ipydex import IPS

# relative to the project root
TESTDATADIR = "base/testdata"

# url of our instance -> id can be used directly
TESTURL1 =  "https://mastodontech.de/@svkd/112469285277802451"

# orinial url of post of other instance -> id can be retrieved via search
TESTURL2 = "https://scicomm.xyz/@ReproducibiliTeaGlobal/112551561515386679"

# url copied from a logged in session on a third isntance ->
# orinial url of post can be retrieved via evaluating 302 (redirect) response
TESTURL3 = "https://social.tchncs.de/deck/@ReproducibiliTeaGlobal@scicomm.xyz/112551561595360270"

# url which does not lead to a valid post
TESTURL4 = "https://mastodonpy.readthedocs.io"

# url with many (>700) responses
# TESTURL5 = "https://hachyderm.io/@molly0xfff/112362200065959056/"
TESTURL5 = "https://mastodontech.de/@molly0xfff@hachyderm.io/112362200985617925"

# TESTURL6 = "https://mastodontech.de/@benni@social.tchncs.de/112682818229282264"
TESTURL6 = "https://mastodontech.de/@benni@social.tchncs.de/112682893210387217"

TESTURL7 = "https://mastodontech.de/@cark@social.tchncs.de/112678881431230219"

# Note: if you want to add new testdata set update_fixtures=True in views.DBCache.find_cached_result(...)


run_slow_tests = os.getenv("RUN_SLOW_TESTS", default="False").lower() == "true"


class TestCore1(TestCase):
    fixtures = ["base/testdata/fixtures01.json"]

    def setUp(self):
        settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = True

    def post_to_view(self, viewname, allow_redirect=True, **kwargs):
        response = self.client.get(reverse(viewname))
        self.assertEqual(response.status_code, 200)

        spec_values=kwargs.get("spec_values", {})

        form = get_first_form(response)
        post_data = generate_post_data_for_form(form, spec_values=spec_values)
        response = self.client.post(form.action_url, post_data)

        if response.status_code == 302 and allow_redirect:
            response = self.client.get(response.url)

        return response

    def test_010_index(self):
        response = self.client.get(reverse("landingpage"))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"utc_landingpage", response.content)

    def test_020_invalid_url(self):
        expected_status = settings.DEFAULT_ERROR_STATUS

        # malformed url
        # -> this does intentionally fail via views.check_valid_url(...)
        # it does not rely on ErrorHandlerMiddleware
        url = reverse("graphviewpage", args=("xxx",))
        response = self.client.get(url)
        self.assertEqual(response.status_code, expected_status)
        self.assertIn(b"utc_malformed_url_error", response.content)

        url = reverse("graphviewpage", args=(TESTURL4,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, expected_status)
        self.assertIn(b"utc_invalid_url_error", response.content)

        # now url which is not malformed but does not exist
        url = reverse("graphviewpage", args=("https://foo123.bar456.zz",))
        response = self.client.get(url)
        self.assertEqual(response.status_code, expected_status)
        self.assertIn(b"utc_unreachable_url", response.content)


    def test_030_nonexistent_url404(self):
        """
        nonexistent url, does rely on ErrorHandlerMiddleware
        """
        rootnodeurl = "https://mastodontech.de/@svkd/0134"

        # let the exception pass through
        settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = False
        with self.assertRaises(Exception) as context:
            response = self.post_to_view("landingpage", spec_values=dict(rootnodeurl=rootnodeurl))

        self.assertTrue(isinstance(context.exception, ValueError))
        self.assertEqual(context.exception.args[1], 404)

        # now the exception should be cateched
        settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = True
        response = self.post_to_view("landingpage", spec_values=dict(rootnodeurl=rootnodeurl))
        self.assertEqual(response.status_code, settings.DEFAULT_ERROR_STATUS)

        # TODO: current error handling is not as originally planned but not critical
        # self.assertIn(b" utc_url/id_not_found ", response.content)

    def test_040_get_graph(self):
        settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = False
        rootnodeurl = TESTURL1
        response = self.post_to_view("landingpage", spec_values=dict(rootnodeurl=rootnodeurl))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"utc_gaphviewpage", response.content)

    # this test is slow. it contains a quite big graph (>700 responses)
    @unittest.skipUnless(run_slow_tests, "omit slow tests")
    def test_041_get_big_graph(self):
        from django.core.management import call_command
        call_command("loaddata", os.path.join(TESTDATADIR, "fixtures02.json"), verbosity=0)
        settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = False
        url1 = reverse("graphviewpage", args=(TESTURL5,))
        response1 = self.client.get(url1)
        self.assertEqual(response1.status_code, 200)


    def test_042_get_small_graph(self):

        # test that no auxiliary node is present here

        url1 = reverse("graphviewpage", args=(TESTURL7,))
        response1 = self.client.get(url1)
        self.assertEqual(response1.status_code, 200)
        self.assertNotIn(b'"is_auxiliary": true', response1.content)


    def test_050_get_graph_with_forced_cache_update(self):
        # settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = True
        url1 = reverse("graphviewpage", args=(TESTURL1,))
        response1 = self.client.get(url1)
        self.assertEqual(response1.status_code, 200)
        self.assertIn(b"utc_gaphviewpage", response1.content)

        cache_metadata1 = get_embedded_json_data(response1, "utjd_cache_metadata")
        self.assertTrue(cache_metadata1["found_in_cache"])
        dt1 = datetime.fromisoformat(cache_metadata1["timestamp_str"])
        diff1 = (datetime.now() - dt1).total_seconds()
        # normally this is much greater but it should also pass if the cache was updated recently
        self.assertGreater(diff1, 300)

        # now force to update the cache
        url2 = f"{url1}?update_cache=True"
        response2 = self.client.get(url2)
        self.assertEqual(response2.status_code, 200)

        cache_metadata2 = get_embedded_json_data(response2, "utjd_cache_metadata")
        self.assertFalse(cache_metadata2["found_in_cache"])
        dt2 = datetime.fromisoformat(cache_metadata2["timestamp_str"])
        diff2 = (datetime.now() - dt2).total_seconds()

        # should be less then 1s  but we allow for some lag
        self.assertLess(diff2, 10)

    def test_060_use_search_function(self):
        url = reverse("graphviewpage", args=(TESTURL2,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("graphviewpage", args=(TESTURL3,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = False
        url = reverse("graphviewpage", args=(TESTURL4,))
        with self.assertRaises(Exception) as context:
            response = self.client.get(url)

        self.assertTrue(isinstance(context.exception, ValueError))
        self.assertIn("FedivisError", repr(context.exception))

    def test_070_allowed_hosts_in_config(self):
        self.assertIn("spov", settings.UT_ALLOWED_HOSTS[0])
        self.assertIn("spov", settings.UT_ALLOWED_HOSTS[1])


ut_allowed_hosts = settings.UT_ALLOWED_HOSTS
@unittest.skipUnless("spov.kddk" in ut_allowed_hosts[0], "unexpected setting")
class TestConfig(TestCase):
    """
    Theses tests are specifically for the main deployment of the developer (spov.kddk.uber.space).
    They can be ignored for other deployments.
    """
    def test_config01(self):
        self.assertEqual(settings.GRAPHVIZ_BIN_OPT, "/home/kddk/opt/gv/bin")


@unittest.skipUnless(execute_browser_tests, "splinter not available")
class TestGUI(StaticLiveServerTestCase):
    # fixtures = ["base/testdata/fixtures01.json"]
    headless = True

    def setUp(self) -> None:
        self.options_for_browser = dict(driver_name='chrome')

        # docs: https://splinter.readthedocs.io/en/latest/config.html
        self.config_for_browser = Config(headless=self.headless)

        self.browsers = []

        return

    def tearDown(self):

        # quit all browser instances (also those which where not created by setUp)
        for browser in self.browsers:
            browser.quit()

    def get_browser_log(self, browser):
        res = browser.driver.get_log("browser")
        browser.logs.append(res)
        return res

    def new_browser(self):
        """
        create and register a new browser

        :return: browser object and its index
        """

        browser = Browser(**self.options_for_browser, config=self.config_for_browser)
        browser.logs = []
        self.browsers.append(browser)

        return browser

    # TODO: fix this test in offline mode (or understand why it should fail, maybe due to images?)
    def test_01_graph_navigation(self):

        b1 = self.new_browser()
        url = reverse("landingpage")
        b1.visit(f"{self.live_server_url}{url}")
        b1.find_by_id("rootNodeUrl")[0].fill(TESTURL1)
        b1.find_by_id("mainSubmit")[0].click()

        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node1")
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node7")
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node7")
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node8")
        self.assertEqual(b1.evaluate_script("currentActiveNode.is_auxiliary"), False)
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node9")
        self.assertEqual(b1.evaluate_script("currentActiveNode.is_auxiliary"), True)
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node2")
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node3")
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node4")
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node5")


        # no child -> nothing happens
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node5")

        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node14")

        # warp arround
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node4")
        send_key_to_browser(b1, Keys.ARROW_LEFT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node14")

        # test upward
        send_key_to_browser(b1, Keys.ARROW_UP)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node13")
        send_key_to_browser(b1, Keys.ARROW_UP)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node12")

        # upward path should be remembered
        send_key_to_browser(b1, Keys.ARROW_UP)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node9")
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node12")
        send_key_to_browser(b1, Keys.ARROW_UP)
        send_key_to_browser(b1, Keys.ARROW_UP)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node1")

        # TODO: fix this test in offline mode (or understand why it should fail)

    def test_02_graph_navigation2(self):

        # test for a bug in horizotnal ordering

        b1 = self.new_browser()
        url1 = reverse("graphviewpage", args=(TESTURL6,))
        b1.visit(f"{self.live_server_url}{url1}")

        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node1")
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        send_key_to_browser(b1, Keys.ARROW_DOWN)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node3")
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node10")
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node12")
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node6")
        send_key_to_browser(b1, Keys.ARROW_RIGHT)
        self.assertEqual(b1.evaluate_script("currentActiveNode.id"), "node3")

# #################################################################################################

# auxiliary functions:

# #################################################################################################


def get_embedded_json_data(response, id):
    soup = BeautifulSoup(response.content.decode("utf8"), "lxml")
    res = soup.find(id=id)
    assert res is not None
    return json.loads(res.string)


def send_key_to_browser(browser, key):
    actions = ActionChains(browser.driver)
    actions.send_keys(key)
    actions.perform()


def get_element_by_html_content(element_list: list, content: str):

    for elt in element_list:
        if elt.html == content:
            res_elt = elt
            return res_elt

    raise ValueError(f"Could not find element with content \"{content}\"")


# helper functions copied from moodpoll
def get_first_form(response):
    """
    Auxiliary function that returns a bs-object of the first form which is specifies by action-url.

    :param response:
    :return:
    """
    bs = BeautifulSoup(response.content, 'html.parser')
    forms = bs.find_all("form")

    form = forms[0]
    form.action_url = form.attrs.get("action")

    return form


def get_form_fields_to_submit(form):
    """
    Return two lists: fields and hidden_fields.

    :param form:
    :return:
    """

    inputs = form.find_all("input")
    textareas = form.find_all("textarea")

    post_fields = inputs + textareas

    types_to_omit = ["submit", "cancel"]

    fields = []
    hidden_fields = []
    for field in post_fields:
        ftype = field.attrs.get("type")
        if ftype in types_to_omit:
            continue

        if ftype == "hidden":
            hidden_fields.append(field)
        else:
            fields.append(field)

    return fields, hidden_fields


def generate_post_data_for_form(form, default_value="xyz", spec_values=None):
    """
    Return a dict containing all dummy-data for the form

    :param form:
    :param default_value:   str; use this value for all not specified fields
    :param spec_values:     dict; use these values to override default value

    :return:                dict of post_data
    """

    if spec_values is None:
        spec_values = {}

    fields, hidden_fields = get_form_fields_to_submit(form)

    post_data = {}
    for f in hidden_fields:
        post_data[f.attrs['name']] = f.attrs['value']

    for f in fields:
        name = f.attrs.get('name')

        if name is None:
            # ignore fields without a name (relevant for dropdown checkbox)
            continue

        if name.startswith("captcha"):
            # special case for captcha fields (assume CAPTCHA_TEST_MODE=True)
            post_data[name] = "passed"
        else:
            post_data[name] = default_value

    post_data.update(spec_values)

    return post_data
