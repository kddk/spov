from django.db import models
from picklefield.fields import PickledObjectField
import secrets

# for the sake of reduced complexity we implement our own serialize methods here
# this should be professionallized in the future

class CacheEntry(models.Model):
    url = models.CharField(max_length=400, blank=False, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    data = PickledObjectField()
