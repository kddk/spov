from django.http import HttpResponse
from django.conf import settings
import traceback
from ipydex import IPS
import fedivis

from .views import render_error_page
from requests.exceptions import ConnectionError
from urllib3.exceptions import MaxRetryError


# in production mode this should not be activated (via settings) for performance reasons
class RequestDebugMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        method = request.META.get("REQUEST_METHOD")
        origin = request.headers.get("Origin")
        host = request.headers.get("Host")
        import time
        time_stamp = time.strftime(r"%Y-%m-%d %H-%M-%S")
        debug_text = f"[{time_stamp}] request: {method} {origin=} {host=}\n"
        print(debug_text)
        with open("_tmp.log", "a") as fp:
            fp.write(debug_text)
        # IPS(print_tb=-1)
        response = self.get_response(request)
        return response


class ErrorHandlerMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response


    def process_exception(self, request, exception):

        final_status = settings.DEFAULT_ERROR_STATUS
        if settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE:
            if isinstance(exception, fedivis.FedivisError):
                assert len(exception.args) >= 2
                errmsg, status = exception.args[:2]
                err_page = render_error_page(request, title="Invalid URL Error", msg=errmsg, status=final_status)

            elif isinstance(exception, ConnectionError) and isinstance(exception.args[0], MaxRetryError):
                status, url = exception.args[1:3]
                errmsg = f"URL {url} could not be reached!"
                err_page = render_error_page(request, title="Unreachable URL", msg=errmsg, status=final_status)

            elif exception:
                err_page = render_error_page(request, title="general exception", msg=repr(exception), status=final_status)

            return err_page
