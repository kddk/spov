import os
from django.views import View
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.conf import settings
from django.forms import ModelForm
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from .models import CacheEntry
from .simple_pages_interface import get_sp, new_sp
from ipydex import IPS
import fedivis


# this serves to understand what happens on the deployment machine
import time
time_stamp = time.strftime(r"%Y-%m-%d %H-%M-%S")
print(f"({time_stamp}) views-module executed")


# envvar HOSTNAME is not set if uwsgi is run via supervisorctl -> manuall way to determine host
# this file hast to be created during deployment
if os.path.isfile(f"/home/{settings.USER}/_this_is_uberspace.txt"):
    # if we are on the deployment host -> use a special version of graphviz
    GRAPHVIZ_BIN = settings.GRAPHVIZ_BIN_OPT
else:
    GRAPHVIZ_BIN = None


class Container:
    pass

class MainView(View):
    def get(self, request):
        context = {
            # nested dict for easier debugging in the template
            "data": {
                "sp": None,
                "unit_test_comment": f"utc_landingpage",
            }
        }

        context["data"]["sp"] = get_sp("landing")
        # template = "base/main_simplepage.html"
        template = "base/main_landingpage.html"

        return render(request, template, context)

    def post(self, request, **kwargs):
        rootnodeurl = request.POST.get("rootnodeurl")
        new_url = reverse("graphviewpage", kwargs=dict(url=rootnodeurl))
        return HttpResponseRedirect(new_url)


class ShowGraphView(View):
    def get(self, request, url):

        # rootnodeurl = request.POST.get("rootnodeurl")
        rootnodeurl = url
        update_cache = request.GET.get("update_cache", "False").lower() == "true"

        errorpage = check_valid_url(request, rootnodeurl)
        if errorpage is not None:
             return errorpage

        return render_graph(request, rootnodeurl, update_cache=update_cache)


def debugpage(request):
    # serve a page via get request to simplify the display of source code in browser


    debug_text = f"debug: {settings.ALLOWED_HOSTS=} {settings.CSRF_TRUSTED_ORIGINS=} {settings.DEVMODE=}"
    print(debug_text)


    if 0:
        rootnodeurl = "https://mastodontech.de/@svkd/112469285277802451"
        return render_graph(request, rootnodeurl)

    context = {"data": {"debug_text": debug_text}}
    template = "base/main_debugpage.html"
    return render(request, template, context)


def render_graph(request, rootnodeurl, update_cache=False, **kwargs):
    # settings.CATCH_EXCEPTIONS_VIA_MIDDLEWARE = False
    rgg = fedivis.ReplyGraphGenerator(spov_mode=True, graphviz_bin=GRAPHVIZ_BIN)
    selection_hashtag = "svkd_test"
    confpath = "config_fedivis.toml"

    # create cache object
    db_cache = DBCache(force_update=update_cache)

    # call preparation of reply graph generator
    rgg._prepare_node_manager(
        url=rootnodeurl, hl_string=selection_hashtag, use_cache=db_cache, confpath=confpath
    )
    rgg._generate_svg_data_from_nm()
    rgg.get_tree_structure_connectors()
    rgg.create_tree_stats(save=False)

    xmin, ymin, xmax, ymax = rgg.viewbox
    xmax1 = min(max(400, xmax), 3000)
    ymax1 = min(max(700, ymax), 6000)
    ymin1 = -0.02*ymax1
    context = {
         "data": {
            "svg_data": rgg.inner_svg_data,
            "svg_viewbox_str": f"{xmin} {ymin1} {xmax1} {ymax1}",
            # number of nodes
            "stats": rgg.tree_stats,
            "cache_metadata": rgg.cache_metadata,
        }
    }
    template = "base/main_graphviewpage.html"
    return render(request, template, context)


def check_valid_url(request, url):

    val = URLValidator()
    res = None
    try:
        val(url)
    except (ValidationError,) as e:
        msg = f"Invalid URL: {url}"
        res = render_error_page(request, title="Malformed URL Error", msg=msg)
    return res


def render_error_page(request, title, msg, status=settings.DEFAULT_ERROR_STATUS):
        sp_type = title.lower().replace(" ", "_")
        sp = new_sp(
            type=sp_type,
            title=title,
            # TODO handle translation (we can not simply use
            # django.utils.translation.gettext here)
            content=msg
        )

        context = {
            # nested dict for easier debugging in the template
            "data": {
                "sp": sp,
                "main_class": "error_container",
                "unit_test_comment": f"utc_{sp_type}",
            }
        }

        template = "base/main_simplepage.html"

        return render(request, template, context, status=status)


# #################################################################################################
# auxiliary functions:
# #################################################################################################

class DBCache(fedivis.FediRequestCache):
    def __init__(self, force_update=False):
        super().__init__()
        self.force_update = force_update

    def find_cached_result(self, url, fallback: callable):
        db_res = CacheEntry.objects.filter(url__exact=url)

        # use this flag for creating and exporting updated fixtures for testdata during development
        update_fixtures = False

        if self.force_update and len(db_res) > 0:
            for db_obj in db_res:
                db_obj.delete()

            db_res = CacheEntry.objects.filter(url__exact=url)
            assert len(db_res) == 0

        if len(db_res) == 0 or update_fixtures:
            res = fallback()

            db_obj = CacheEntry(url=url, data=res)
            db_obj.save()
            res["__cache_metadata__"] = {"timestamp": db_obj.timestamp, "found_in_cache": False}

            if update_fixtures:
                # the file
                from django.core import management
                management.call_command('dumpdata', 'base.CacheEntry', indent="  ", stdout=open('test_data.json', 'w'))
        else:
            db_obj = db_res.first()
            res = db_obj.data
            res["__cache_metadata__"] = {"timestamp": db_obj.timestamp, "found_in_cache": True}

        return res
