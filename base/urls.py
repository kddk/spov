from django.urls import path

from . import views


main_view = views.MainView.as_view

urlpatterns = [
    path("", main_view(), name="landingpage"),
    path("show/<path:url>/", views.ShowGraphView.as_view(), name="graphviewpage"),
    # path("doc/<slug:slug>/<slug:doc_key>", main_view(), name="reviewpage", kwargs={"mode": "review"}),
    path("debug", views.debugpage, name="debugpage"),
]
