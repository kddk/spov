# spov

Spov is a web based software for selected post view. Its primary purpose is to display fediverse posts (nodes in a tree) which fulfill certain conditions such as:

- having a specified root node,
- containing specified hashtags,
- being authored by specified accounts.

The main usecase is to facilitate constructive debates by only displaying those posts which comply with certain rules. The whole concept is described at [https://kddk.eu/selbstverpflichtung](https://kddk.eu/selbstverpflichtung).

## Manual Testing


- `python manage.py makemigrations`

- Delete old db (cache): `rm -f db.sqlite3; python manage.py flush; python manage.py migrate --run-syncdb`

- `python manage.py runserver`

## Unittests

`pytest` (recommended: splinter is installed and configured)

## Deployment

- First time:
    - adapt config
    - make sure graphviz version is at least 2.46.1 (tested)
    - `python deployment/deploy.py --help`
    - `python deployment/deploy.py --initial remote`
- Update:
    - `python deployment/deploy.py remote`

### The Graphviz Problem

The current version of graphviz on uberspace is too old and has undesired behavior for node-ordering.
To overcome this it it necessary to download ([from here](https://graphviz.org/download/source/)) and compile graphviz manually:

```
./configure --prefix=$HOME/opt/gv
make
make install
```

(successfully tested with version 2.46.1)

### Feedback

Contact me <https://cknoll.github.io/pages/impressum.html>
